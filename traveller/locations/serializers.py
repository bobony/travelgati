from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Place, Attraction, Category, Country

class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields=['name']

class CountrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country
        fields=['country', 'state']

class AttractionSerializer(serializers.HyperlinkedModelSerializer):
    category = CategorySerializer(many=True)
    location = CountrySerializer()
    class Meta:
        model = Attraction
        fields=('name','slug', 'link', 'category','location', 'image')
        lookup_field = 'slug'

class PlaceSerializer(serializers.HyperlinkedModelSerializer):
    attraction = AttractionSerializer(many=True)
    location = CountrySerializer()
    class Meta:
        model = Place
        fields=(
        'name',
        'slug',
        'description',
        'hero_image',
        'link',
        'location',
        'nearest_place',
        'km_from_np',
        'attraction',
        'time_to_visit',
        'order',
        'yt_video',
        'must_visit',
        )
