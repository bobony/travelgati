from rest_framework.generics import ListAPIView, RetrieveAPIView
from locations.models import Attraction, Place
from .serializers import PlaceSerializer, AttractionSerializer
class AttractionListAPIView(ListAPIView):
    queryset = Attraction.objects.all()
    serializer_class  = AttractionSerializer
class AttractionDetailAPIView(RetrieveAPIView):
    queryset = Attraction.objects.all()
    serializer_class  = AttractionSerializer
class PlaceListAPIView(ListAPIView):
    queryset = Place.objects.all()
    serializer_class  = PlaceSerializer
class PlaceDetailAPIView(RetrieveAPIView):
    queryset = Place.objects.all()
    serializer_class  = PlaceSerializer
