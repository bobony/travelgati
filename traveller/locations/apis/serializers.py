from rest_framework import serializers
from rest_framework.reverse import reverse

from locations.models import Place, Attraction, Category, Country

class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields=['name']

class CountrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country
        fields=['country', 'state']
attraction_detail_url = serializers.HyperlinkedIdentityField(
        view_name='apis:attraction-detail',
        lookup_field='slug'
        )

class AttractionSerializer(serializers.HyperlinkedModelSerializer):
    url = attraction_detail_url
    category = CategorySerializer(many=True)
    location = CountrySerializer()
    class Meta:
        model = Attraction
        fields=('url', 'name','slug', 'link', 'category','location', 'image')

place_detail_url = serializers.HyperlinkedIdentityField(
        view_name='apis:place-detail',
        lookup_field='slug'
        )
class AttractionDetailSerializer(serializers.HyperlinkedModelSerializer):
    category = CategorySerializer(many=True)
    location = CountrySerializer()
    class Meta:
        model = Attraction
        fields=['name','slug', 'link', 'category','location', 'image']


class PlaceDetailSerializer(serializers.ModelSerializer):
    url = place_detail_url
    attraction = AttractionSerializer(many=True)
    location = CountrySerializer()
    class Meta:
        model = Place
        fields=(
        'name',
        'slug',
        'description',
        'hero_image',
        'link',
        'location',
        'nearest_place',
        'km_from_np',
        'attraction',
        'time_to_visit',
        'order',
        'yt_video',
        'must_visit',
        )
class PlaceSerializer(serializers.HyperlinkedModelSerializer):
    url = place_detail_url
    attraction = AttractionSerializer(many=True)
    location = CountrySerializer()
    class Meta:
        model = Place
        fields=(
        'url',
        'name',
        'slug',
        #'description',
        'hero_image',
        'location',
        'nearest_place',
        'attraction',
        'order',
        'must_visit',
        )
