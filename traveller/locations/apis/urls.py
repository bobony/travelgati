from django.conf.urls import url
from django.contrib import admin

from .views import (
AttractionListAPIView,
AttractionDetailAPIView,
PlaceListAPIView,
PlaceDetailAPIView
)

urlpatterns = [
    url(r'attraction/', AttractionListAPIView.as_view(), name='attarction-list'),
    url(r'attraction/(?P<slug>[\w-]+)/$', AttractionDetailAPIView.as_view(), name='attraction-detail'),
    url(r'place/', PlaceListAPIView.as_view(), name='place-list'),
    url(r'place/(?P<slug>[\w-]+)/$', PlaceDetailAPIView.as_view(), name='place-detail'),

]
