from rest_framework.routers import DefaultRouter
from django.conf.urls import include, url
from .import views

router = DefaultRouter()
router.register(r'places',views.PlaceViewSet)
router.register(r'attractions',views.AttractionViewSet)
# urlpatterns = [
#     url(r'^places/(?P<slug>[\w-]+)/$', views.PlaceViewSet, name='place-details'),
#     url(r'^attraction/(?P<slug>[\w-]+)/$', views.AttractionViewSet, name='attraction-detail'),
# ]
#
# urlpatterns += router.urls
