from django.db import models

class Place(models.Model):
    name = models.CharField(max_length=250)
    location = models.CharField(max_length=250)
    order = models.CharField(max_length=250)
    slug= models.SlugField(max_length=250)
    description = models.TextField(null=True, blank=True)
    hero_image = models.ImageField(upload_to='locations')
    link = models.CharField(max_length=500, null=True, blank=True)
    nearest_place = models.CharField(max_length=250)
    km_from_np = models.CharField(max_length=250, null=True, blank = True)
    location = models.ForeignKey('Country')
    attraction = models.ManyToManyField('Attraction')
    time_to_visit = models.CharField(max_length=250, null=True, blank = True)
    yt_video = models.CharField(max_length=250, null=True, blank = True)
    must_visit = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return str(self.name)

class Attraction(models.Model):
      name = models.CharField(max_length=250)
      description = models.TextField(null=True, blank=True)
      order = models.CharField(max_length=250)
      slug= models.SlugField(max_length=250)
      link = models.CharField(max_length=500)
      location = models.ForeignKey('Country')
      nearest_place = models.CharField(max_length=250, null=True, blank = True)
      km_from_np = models.CharField(max_length=250, null=True, blank = True)
      category = models.ManyToManyField('Category')
      image = models.ImageField(upload_to='locations')
      must_visit = models.BooleanField(default=False)
      def __str__(self):
          return self.name

class Category(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Country(models.Model):
    country_id = models.IntegerField()
    country = models.CharField(max_length=200)
    state = models.CharField(max_length=250)

    def __str__(self):
        return self.country + ' - ' + self.state
