from django.shortcuts import render, render_to_response,render, HttpResponse, HttpResponseRedirect, get_object_or_404
from django_filters import rest_framework as filters
from rest_framework import viewsets

from .models import Place, Attraction
from .serializers import PlaceSerializer, AttractionSerializer

class PlaceViewSet(viewsets.ModelViewSet):
    #def list(self, request, *args, **kwargs):
    #    response = super(PageViewSet, self).list(request, *args, **kwargs) # call the original 'list'
    #    response.data = {"site": response.data} # customize the response data
    #    return response # return response with this custom representation
    queryset = Place.objects.all()
    serializer_class  = PlaceSerializer
    lookup_field = 'slug'
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('location',)
class AttractionViewSet(viewsets.ModelViewSet):
    #def list(self, request, *args, **kwargs):
    #    response = super(PageViewSet, self).list(request, *args, **kwargs) # call the original 'list'
    #    response.data = {"site": response.data} # customize the response data
    #    return response # return response with this custom representation
    queryset = Attraction.objects.all()
    serializer_class  = AttractionSerializer
    lookup_field = 'slug'

def home(request):

    return render_to_response('index.html', locals())
