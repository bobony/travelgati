from django.contrib import admin

from .models import Place, Attraction, Category, Country

admin.site.register(Place)
admin.site.register(Attraction)
admin.site.register(Category)
admin.site.register(Country)
